### 简介
springcloud 示例代码
### 服务注册中心
eureka.crex1.cn
### 分支说明
master 服务提供

consumer 服务消费

uploaddemo 接收文件服务服务

uploaddemo-consumer 发送文件服务 测试调用服务

hystrix 服务保护

hystrix-dashboard 服务监控

zuul服务网关
### 服务说明
springcloud-server-client 提供服务，比如提供获取服务列表服务

springcloud-server-consumer 消费指定的服务 比如调用服务列表服务 获取服务列表

springcloud-server-upload 文件接收

springcloud-upload-consumer 文件发送

springcloud-hystrix-consumer 测试服务保护

springcloud-hystrix-dashboard hystrix监控面板

springcloud-server-api-gateway 服务网关
### 示例API
http://localhost:8080/dc 返回一个服务列表

http://localhost:8088/consumer 根据服务名获取服务实例 消费(调用)服务接口

http://localhost:8083/ribbonconsumer ribbon接口 测试服务保护

http://localhost:8083/feignconsumer feign接口 测试服务保护

http://localhost:8088/ribbonconsumer ribbon接口调用服务

http://localhost:8088/feignconsumer feign接口调用服务

http://localhost:8084/hystrix/ 监控面板

http://localhost:8888/springcloud-server-client/dc zuul服务网关调用服务

http://localhost:8888/server-client/dc 手动创建映射调用服务

http://localhost:8888/server-client/dc?accessToken 带权限检验请求示例

http://localhost:8888/swagger-ui.html 文档汇总