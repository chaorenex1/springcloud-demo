package cn.crex1.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.discovery.EurekaClient;
import com.spring4all.swagger.EnableSwagger2Doc;

/**  
* <p>title: DcController</p>  
* <p>description: </p>  
* @author zzh  
* @date 2018年7月19日  
*/
@EnableSwagger2Doc
@RestController
//获取服务清单
public class DcController {

	@Autowired
//	private EurekaClient eurekaClient;
	private DiscoveryClient discoveryClient;//eureka服务提供者接口
	
	@GetMapping("/dc")
	public String dc() {
//		获取服务
//		try {
//			Thread.sleep(5000L);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		//线程睡眠 配合hystrix使用 模拟请求超时，触发服务保护，进行服务降级
		String services="Services: "+discoveryClient.getServices();
//		String services="service: "+eurekaClient.getApplications().getRegisteredApplications();
		System.out.println(services);
		return services;
	}
}
